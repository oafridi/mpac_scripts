/*
Title: Archiving add-ons script

This is split into several sections.

Steps to follow
1. Run (A) to update any PvA to PvV
2. Via UI archive the add-on
3. Reset archived versions back to PvV using (B)

Marketplace$ = paid via atlassian
OffMarket$ = paid via vendor

*/

// (A) Change all PvA versions to PvV

var plugin_name = 'com.kintosoft.jira.subversion-plus' // Enter plugin key i.e. 'com.intenso.jira.plugins.magic-actions'

var a = db.plugins.findOne({pluginKey: plugin_name});
var b = db.plugins.findOne({pluginKey: plugin_name});
var c = []

for (var i = 0; i< a.pvs.versions.length; i++) {
  if (a.pvs.versions[i].marketplaceType._typeHint == 'com.atlassian.marketplace.model.Marketplace$') {
    c.push(i)
    print("PvA: " + a.pvs.versions[i].version + ' ' +a.pvs.versions[i].marketplaceType._typeHint)
    a.pvs.versions[i].marketplaceType._typeHint = 'com.atlassian.marketplace.model.OffMarket$';
  }
}

db.plugins.update({pluginKey: plugin_name},a);

print("All PvA versions have been changed to PvV, go ahead and archive the add-on from the UI");

// (B) - reset version to PvV

var z = db.archivedPlugins.findOne({pluginKey: plugin_name});

for (var i = 0; i< z.pvs.versions.length; i++) {
  print(z.pvs.versions[i].marketplaceType._typeHint, z.pvs.versions[i].version);
}

for (var i = 0; i< c.length; i++) {
  z.pvs.versions[c[i]].marketplaceType._typeHint = 'com.atlassian.marketplace.model.Marketplace$';
}

db.archivedPlugins.update({pluginKey: plugin_name},z);