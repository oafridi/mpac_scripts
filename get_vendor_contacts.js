rs.slaveOk();

var a = db.vendors.find({'verified.status._typeHint': 'com.atlassian.marketplace.model.VerifiedApproved$'});

// Prints name of vendor and comma sep list of vendor contacts (separated by a semicolon)
a.forEach(function(x) {

  var vendors = [];
  for(var i=0; i<x.users.length; i++) {
    var vendor = db.users.findOne({"_id":x.users[i]});
    if (vendor.email != "") {
      vendors.push(vendor.email);
    }
  }
  var list = vendors.join();
  print(x.name + ";" + list);
})